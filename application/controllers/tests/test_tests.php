<?php
require_once(APPPATH . '/controllers/tests/Toast.php');

class Test_tests extends Toast
{
	function __construct()
	{
		parent::__construct(__FILE__);
		// Load any models, libraries etc. you need here
	}

	/**
	 * OPTIONAL; Anything in this function will be run before each test
	 * Good for doing cleanup: resetting sessions, renewing objects, etc.
	 */
	function _pre() {
		
		$this->load->model('user');

	}

	/**
	 * OPTIONAL; Anything in this function will be run after each test
	 * I use it for setting $this->message = $this->My_model->getError();
	 */
	function _post() {}


	/* TESTS BELOW */

	function test_assertCount()
	{
		$this->assertCount(1, array('foo'));
	}

	function test_assertNotCount()
	{
		$this->assertNotCount(1, array('foo'), 'AAAaaaarrrrg!');
	}

	function test_assertEmpty()
	{
		$this->assertEmpty(array());
	}

	function test_assertNotEmpty()
	{
		$this->assertNotEmpty(array('foo'));
	}

	function test_assertEquals()
	{
		$this->assertEquals('bar', 'bar');
	}

	function test_assertNotEquals()
	{
		$this->assertNotEquals('bar', 'baz');
	}

	function test_assertFalse()
	{
		$this->assertFalse(0);
	}

	function test_assertGreaterThan()
	{
		$this->assertGreaterThan(1, 5);
	}

	function test_assertGreaterThanOrEqual()
	{
		$this->assertGreaterThanOrEqual(2, 2);
	}

	function test_assertInstanceOf()
	{
		$this->assertInstanceOf('stdClass', new stdClass);
	}

	function test_assertInternalType()
	{
		$this->assertInternalType('string', 42);
	}

	function test_assertLessThan()
	{
		$this->assertLessThan(1, 0);
	}

	function test_assertLessThanOrEqual()
	{
		$this->assertLessThanOrEqual(1, 1);
	}

	function test_assertNull()
	{
		$this->assertNull(NULL);
	}

	function test_assertNotNull()
	{
		$this->assertNotNull('NULL');
	}

	function test_assertObjectHasAttribute()
	{
		$foo = new stdClass;
		$foo->foo = 'bar';
		$this->assertObjectHasAttribute('foo', $foo);
	}

	function test_assertObjectNotHasAttribute()
	{
		$this->assertObjectNotHasAttribute('foo', new stdClass);
	}

	function test_assertTrue()
	{
		$this->assertTrue(TRUE);
	}
     
    function test_fail_me()
	{
		$this->assertTrue(FALSE, 'I expect this to fail.');
	}

	


}

// End of file example_test.php */
// Location: ./system/application/controllers/test/example_test.php */