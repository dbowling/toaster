<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user');
	} 

	public function index()
	{
		//example from of http://seejohncode.com/2011/02/12/codeigniter-model-instances
		

		$this->load->view('users');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */