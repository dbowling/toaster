<?php
class User extends CI_Model {
	const TABLE_NAME = 'users';

	private static $CI;

	public $id;
    public $firstname;
    public $lastname;
    public $email;

    public function __construct() { 
        parent::__construct(); 

        self::$CI = get_instance();
    }



    static function get_all() {

        return self::$CI->db->get(self::TABLE_NAME)->result(__class__);
    }

    public function get_by_email($email) {
        return $this->db->where('email', $email)->get(self::TABLE_NAME)->row(0, __class__);
    }

    public function comments() {
        $this->load->model('comment');
        return $this->comment->get_by_user_id($this->id);
    }

    public function save()
    {
    	self::$CI->where('id', $this->id);
    	return self::$CI->update(self::TABLE_NAME, $this);
    }

    public function full_name() {
    	return "{$this->firstname} {$this->lastname}";
    }
}