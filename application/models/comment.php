<?php
class Comment extends CI_Model {
    const TABLE_NAME = 'comments';

    private static $CI;

	var $id;
    var $firstname;
    var $lastname;
    var $email;

    public function __construct() { 
	    parent::__construct(); 

	    self::$CI = get_instance();
	}

    public function get_by_user_id($user_id) {
        return $this->db->where('user_id', $user_id)->get(self::TABLE_NAME)->result(__class__);
    }
}