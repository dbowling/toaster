</div>

<p id="author">TOASTER by Dan Bowling. Based on TOAST, by Jens Roland.</p>

<script>
	

	function init(){
		console.log('Calculating Results Summary');

		var passes = count_passes();
		var fails = count_failures();


		document.getElementById('totaltestscount').innerHTML = fails + passes;
		document.getElementById('failedtestscount').innerHTML = fails;
		document.getElementById('passedtestscount').innerHTML = passes;

		if(fails < 1){
			document.getElementById('buildstatus').innerHTML = 'good ;)';
		}else{
			document.getElementById('buildstatus').innerHTML = 'broken!';
		}
		
		
		highlite_result_block(fails);

		if(total_number_of_tests_run(passes, fails) == 0) alert('Did you write any tests?');

		console.log('Done');

	};

	function count_passes(){
		return document.getElementsByClassName('passedtest').length;
	}

	function count_failures(){
		return document.getElementsByClassName('failedtest').length;
	}

	function highlite_result_block(fails){
		if(fails > 0){
			document.getElementById('summary').className = 'err';
		}else{
			document.getElementById('summary').className = 'pas';
		}


	}

	function total_number_of_tests_run(passes, fails){
		return passes + fails;
	}

</script>

</body>
</html> 