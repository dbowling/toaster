<html>
<head>
<title>Unit test results</title>
<style type="text/css">
body {padding: 0 15px; font-family: Arial, sans-serif; font-size: 9pt; background-color: #fff; }

#summary { 
	font-size: 11pt;
	padding: 10px; 
	margin: 10px 0;
	-webkit-box-shadow: 2px 1px 3px 1px #ddd;
	-moz-box-shadow: 2px 1px 3px 1px #ddd;
	box-shadow: 2px 1px 3px 1px #ddd;
	-webkit-transition: background .5s ease-in;
	-moz-transition: background .5s ease-in;
	-o-transition: background .5s ease-in;
	transition: background .5s ease-in;
}

#summary .firstline { font-size: 13pt; margin-bottom: 10px;}
#summary span { font-weight: bold;};


#results { width: 100%;}
#header {
	width: 100%;
	margin: 0;
	border-bottom: 1px solid #ccc;
}
.err, .pas { color: white; font-weight: normal; margin: 2px 0; padding: 2px 5px; vertical-align: top; }
.err { background-color: #C90000; border: 1px solid #960C0C;}
.pas { background-color: #008011; border: 1px solid #005400;}

.passedtest { display: none; }

.failedtest { 
	background-color: #EDEDED; 
	color: #000; 
	border: 1px solid #616161; 
	border-top: 4px solid #960C0C; 
	padding: 10px; 
	margin: 10px 0;
	-webkit-box-shadow: 2px 1px 3px 1px #ddd;
	-moz-box-shadow: 2px 1px 3px 1px #ddd;
	box-shadow: 2px 1px 3px 1px #ddd;



}

.failedtest:hover { 
	background-color: #FAF9F0; 
	color: #000; 
	border: 1px solid #000; 
	border-top: 4px solid #960C0C; 
	-webkit-box-shadow: 2px 1px 3px 1px #ddd;
	-moz-box-shadow: 2px 1px 3px 1px #ddd;
	box-shadow: 2px 1px 3px 1px #ddd;

	-webkit-transition: background .4s ease-out;
	-moz-transition: background .4s ease-out;
	-o-transition: background .4s ease-out;
	transition: background .4s ease-out;
}

.failedtest a { font-family: monospace; text-decoration: none;}

.failedtest a:link,
.failedtest a:visited{ 
	border: 1px solid #CCCCCC; 
	background-color: #E6E6E6; 
	padding: 3px;
	color: #474747;
}

.failedtest a:hover,
.failedtest a:active { 
	border: 1px solid #9C9C9C; 
	background-color: #F7F5F6; 
	padding: 3px;
	color: #000;
}

.failedtest span { color: #fff; background-color: #960C0C; }


.detail { padding: 12px 0 8px 20px; }
h1 { font-size: 12pt }
a:link, a:visited {  color: black }
a:active, a:hover {  color: black; background-color: yellow }
h1 {
	line-height: 50px;
	font-size: 20px;
	margin: 0;
}

#author { color: #828282; text-align: center; padding: 30px 0;}

</style>
</head>
<body onload="init()">

<div id="header">

<h1><span style="color: #C90000">TOAST</span><span style="color: #ccc;">ER</span> - Enhanced Unit Testing for CodeIgniter</h1>

</div>

<div id="summary">
	<div class="firstline">This build is <span id="buildstatus"></span></div>
	<div><span id="totaltestscount"></span> tests where ran in <?php echo $this->benchmark->elapsed_time('total_execution_time_start', 'total_execution_time_end');?> seconds.</div>
	<div>Tests Passed: <span id="passedtestscount"></span></div>
	<div>Tests Failed: <span id="failedtestscount"></span></div>
</div>

<div id="results">