<?php $this->load->helper('language'); ?>

<?php $i=0; foreach ($results as $result): ?>

	<?php if ($result[lang('ut_result')] != lang('ut_passed')): ?>
		<div class="failedtest">

			<strong><?php echo $result[lang('ut_test_name')]; ?></strong>

			<?php if ( ! empty($messages[$i])): ?>
			<div class="detail">
				<?php echo $messages[$i]; ?>&nbsp;
			</div>
			<?php endif; ?>

		</div>
	<?php else: ?>
		<div class="passedtest"></div>
	<?php endif; ?>

<?php $i++; endforeach; ?>

